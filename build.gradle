import groovy.io.FileType

import java.nio.file.Files
import java.nio.file.Paths

buildscript {
    repositories {
        mavenCentral()
        maven {
            url "https://plugins.gradle.org/m2/"
        }
    }

    dependencies {
        classpath 'net.java.dev.jna:jna:5.5.0'
        classpath "gradle.plugin.nl.javadude.gradle.plugins:license-gradle-plugin:0.14.0"
    }
}

apply plugin: 'java-library'
apply plugin: "maven-publish"

apply plugin: 'idea'
apply plugin: 'com.github.hierynomus.license'

tasks.withType(JavaCompile) {
    options.encoding = 'UTF-8'
}

javadoc {
    options.setEncoding 'UTF-8'

    if(JavaVersion.current().isJava9Compatible()) {
        options.addBooleanOption('html5', true)
    }
}

repositories {
    mavenCentral()
}

dependencies {
    api 'net.java.dev.jna:jna:5.5.0'
    testCompile 'junit:junit:4.12'
}

if (project == getRootProject()) {
    wrapper {
        distributionType Wrapper.DistributionType.ALL
        gradleVersion '6.1.1'
    }
}

group "de.uni_passau.fim.seibt"

if (System.getenv("GITLAB_CI") != null) {
    task sourcesJar(type: Jar) {
        from sourceSets.main.allJava
        archiveClassifier = 'sources'
    }

    task javadocJar(type: Jar) {
        from javadoc
        archiveClassifier = 'javadoc'
    }

    final CI_COMMIT_TAG = System.getenv('CI_COMMIT_TAG')
    final CI_JOB_MANUAL = System.getenv('CI_JOB_MANUAL') != null
    
    if (!CI_JOB_MANUAL && CI_COMMIT_TAG != null) {
        // Match tags like 'v0.28.1_1' and extract 0.28.1_1 as the version number of the artifact.
        // If we are not building a manual snapshot the short SHA of the commit being built is used.
        final VERSION_MATCHER = (CI_COMMIT_TAG =~ /v?((?:0|[1-9]\d*)\.(?:0|[1-9]\d*)\.(?:0|[1-9]\d*)_(?:0|[1-9]\d*))/)

        if (VERSION_MATCHER.matches() && VERSION_MATCHER.groupCount() == 1 && 
            VERSION_MATCHER.group(1) != null && !VERSION_MATCHER.group(1).isEmpty()) {
            
            project.version = VERSION_MATCHER.group(1)
        } else {
            throw new GradleScriptException('Invalid version tag \'' + CI_COMMIT_TAG + '\'')
        }
    } else {
        project.version = System.getenv('CI_COMMIT_SHORT_SHA')
    }

    publishing {

        publications {
            JNativeMerge(MavenPublication) {
                final BASE_ID = 'jnativemerge'

                if (CI_JOB_MANUAL) {
                    artifactId = BASE_ID + '-snapshot'
                } else {
                    artifactId = BASE_ID
                }

                from components.java
                artifact sourcesJar
                artifact javadocJar

                pom {
                    name = 'JNativeMerge'
                    description = 'A JNA based binding to libgit2 that provides the equivalent of the `git merge-file` command.'
                    url = 'https://gitlab.infosun.fim.uni-passau.de/seibt/JNativeMerge'
                    licenses {
                        license {
                            name = 'GNU Lesser General Public License V3'
                            url = 'https://www.gnu.org/licenses/lgpl-3.0.de.html'
                        }
                    }
                    developers {
                        developer {
                            id = 'seibt'
                            name = 'Georg Seibt'
                            email = 'seibt@fim.uni-passau.de'
                        }
                    }
                    scm {
                        connection = 'scm:git:https://gitlab.infosun.fim.uni-passau.de/seibt/JNativeMerge.git'
                        developerConnection = 'scm:git:ssh://git@gitlab.infosun.fim.uni-passau.de:seibt/JNativeMerge.git'
                        url = 'https://gitlab.infosun.fim.uni-passau.de/seibt/JNativeMerge'
                    }
                }
            }
        }

        repositories {
            maven {
                name 'GitLab'
                url "${System.getenv('CI_API_V4_URL')}/projects/${System.getenv('CI_PROJECT_ID')}/packages/maven"
                credentials(HttpHeaderCredentials) {
                    name "Job-Token"
                    value System.getenv('CI_JOB_TOKEN')
                }
                authentication {
                    header(HttpHeaderAuthentication)
                }
            }
        }
    }
}

task includeNativeLib(type: Copy) {
    final LIBGIT2_LOC_PROP = 'jnativemerge.libgit2.path'
    final LIBGIT2_SEARCH_PROP = 'jnativemerge.libgit2.search'

    final LIB_EXPECTED_NAME = Paths.get(System.mapLibraryName('git2'))

    if (!project.hasProperty(LIBGIT2_LOC_PROP)) {
        // Whether the required libgit2 binary is included in the repository.
        def built_in = sourceSets.main.resources.sourceDirectories.any {dir ->
            Files.exists(dir.toPath().resolve(com.sun.jna.Platform.RESOURCE_PREFIX).resolve(LIB_EXPECTED_NAME))
        }

        if (built_in) {
            logger.lifecycle('A libgit2 version appropriate for the current platform is built-in.')
            return
        }

        logger.log(LogLevel.ERROR, 'No appropriate libgit2 version built-in.')
        logger.lifecycle('No libgit2 path provided using the Gradle property \'' + LIBGIT2_LOC_PROP + '\'.') 

        if (project.hasProperty(LIBGIT2_SEARCH_PROP)) {
            logger.lifecycle('Attempting to find the libgit2 native library in the Java library search path.')

            final JVM_LIB_PATH = 'java.library.path'
            def searchPaths = System.getProperty(JVM_LIB_PATH).tokenize(System.getProperty('path.separator'))

            for (String dir : searchPaths) {
                File searchDir = new File(dir)

                if (!(searchDir.exists() && searchDir.isDirectory())) {
                    logger.lifecycle('Skipping invalid library search directory ' + searchDir.absolutePath)
                    continue
                } else {
                    logger.lifecycle('Checking ' + searchDir.absolutePath)
                }

                List<File> foundNativeLibs = []

                searchDir.eachFileRecurse(FileType.FILES) {
                    if (it.name == LIB_EXPECTED_NAME.toString()) {
                        foundNativeLibs.add(it)
                    }
                }

                if (foundNativeLibs.size() > 0) {
                    File libFile = foundNativeLibs[0]
                    logger.lifecycle('Discovered ' + libFile + ' in the Java library search path.')
                    project.ext.set(LIBGIT2_LOC_PROP, libFile.absolutePath)
                    break
                }
            }
        } else {
             logger.lifecycle('You may enable searching in the JVM library path by setting the Gradle ' +
                              'property \'' + LIBGIT2_SEARCH_PROP + '\'.')
        }
    }

    if (project.hasProperty(LIBGIT2_LOC_PROP)) {
        final LIB_PATH = Paths.get(project.getProperty(LIBGIT2_LOC_PROP)).toAbsolutePath()

        final LIB_INCLUDE_RES_DIR = buildDir.toPath().resolve('nativeRes')
        final LIB_PLATFORM_DIR = LIB_INCLUDE_RES_DIR.resolve(com.sun.jna.Platform.RESOURCE_PREFIX)

        if (LIB_PATH.fileName == LIB_EXPECTED_NAME) {
            logger.lifecycle('Including \'' + LIB_PATH + '\' in the JAR under \'' + LIB_PLATFORM_DIR + '\'')
        } else {
            throw new GradleException('File \'' + LIB_PATH + '\' does not have the expected name \'' + LIB_EXPECTED_NAME + '\'')
        }

        doFirst {
            if (Files.exists(LIB_INCLUDE_RES_DIR)) {
                if (Files.isDirectory(LIB_INCLUDE_RES_DIR)) {
                    if (!LIB_INCLUDE_RES_DIR.deleteDir()) throw new GradleException("Can not delete " + LIB_INCLUDE_RES_DIR)
                } else {
                    if (!LIB_INCLUDE_RES_DIR.delete()) throw new GradleException("Can not delete " + LIB_INCLUDE_RES_DIR)
                }
            }
        }

        sourceSets.main.resources {
            srcDir LIB_INCLUDE_RES_DIR
        }

        from LIB_PATH
        into LIB_PLATFORM_DIR
    } else {
        logger.log(LogLevel.ERROR, 'The produced JAR will not contain a libgit2 version appropriate for the current ' +
                                   'platform. You must make one available to JNA for this library to function.')
    }
}

processResources.dependsOn includeNativeLib

idea {
    module {
        downloadJavadoc = true
        downloadSources = true
    }
}

license {
    header = file('LICENSE_HEADER')
    strictCheck = true

    def resPath = project.projectDir.toPath().resolve("src/main/resources")
    resPath.eachFileRecurse(FileType.FILES) {
        exclude resPath.relativize(it).toString()
    }
}

test {
    dependsOn cleanTest

    testLogging {
        events "passed", "skipped", "failed"
    }
}
