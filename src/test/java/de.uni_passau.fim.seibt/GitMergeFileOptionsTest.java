/**
 * Copyright (C) 2019 Georg Seibt
 *
 * This file is part of JNativeMerge.
 *
 * JNativeMerge is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JNativeMerge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JNativeMerge. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.seibt;

import de.uni_passau.fim.seibt.GitMergeFileOptions.Favor;
import de.uni_passau.fim.seibt.GitMergeFileOptions.Flag;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static de.uni_passau.fim.seibt.GitMergeFileOptions.Flag.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GitMergeFileOptionsTest {

    private GitMergeFileOptions opts;

    @Before
    public void setUp() {
        this.opts = new GitMergeFileOptions();
    }

    @Test
    public void getFavor() {
        for (Favor favor : Favor.values()) {
            opts.favor = favor.getId();
            assertEquals("GitMergeFileOptions#getFavor failed for " + favor, favor, opts.getFavor());
        }
    }

    @Test
    public void setFavor() {
        for (Favor favor : Favor.values()) {
            opts.setFavor(favor);
            assertEquals("GitMergeFileOptions#setFavor failed for " + favor, favor.getId(), opts.favor);
        }
    }

    @Test
    public void getFlags() {

        String msg = "The default flag is not returned from a new GitMergeFileOptions instance.";
        assertEquals(msg, new HashSet<>(singletonList(GIT_MERGE_FILE_DEFAULT)), opts.getFlags());

        Set<Flag> expectedFlags = new HashSet<>(asList(GIT_MERGE_FILE_IGNORE_WHITESPACE,
                                                       GIT_MERGE_FILE_DIFF_PATIENCE,
                                                       GIT_MERGE_FILE_SIMPLIFY_ALNUM));

        for (Flag expectedFlag : expectedFlags) {
            opts.flags |= expectedFlag.getId();
        }

        msg = "Combined flags are not returned correctly.";
        assertEquals(msg, expectedFlags, opts.getFlags());

        for (Flag flag : Flag.values()) {
            opts = new GitMergeFileOptions();
            opts.flags = flag.getId();

            Set<Flag> setFlags = opts.getFlags();
            msg = "The flag " + flag + " is not returned correctly when used independently.";
            assertTrue(msg, setFlags.size() == 1 && setFlags.contains(flag));
        }
    }

    @Test
    public void setFlags() {

        opts.setFlags(GIT_MERGE_FILE_IGNORE_WHITESPACE, GIT_MERGE_FILE_DIFF_PATIENCE, GIT_MERGE_FILE_SIMPLIFY_ALNUM);

        String msg = "Combined flags are not set correctly.";
        assertEquals(msg, opts.flags, GIT_MERGE_FILE_IGNORE_WHITESPACE.getId() |
                                      GIT_MERGE_FILE_DIFF_PATIENCE.getId() |
                                      GIT_MERGE_FILE_SIMPLIFY_ALNUM.getId());

        for (Flag flag : Flag.values()) {
            opts = new GitMergeFileOptions();
            opts.setFlags(flag);

            msg = "The flag " + flag + " is not set correctly.";
            assertEquals(msg, opts.flags, flag.getId());
        }
    }
}