/**
 * Copyright (C) 2019 Georg Seibt
 *
 * This file is part of JNativeMerge.
 *
 * JNativeMerge is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JNativeMerge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JNativeMerge. If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni_passau.fim.seibt;

import org.junit.BeforeClass;
import org.junit.Test;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;

public class LibGit2Test {

    /**
     * Set the expected native library version here. Negative numbers indicate "Don't Care".
     */
    private static final int GIT2_EXPECTED_MAJOR = 0;
    private static final int GIT2_EXPECTED_MINOR = 28;
    private static final int GIT2_EXPECTED_PATCH = -1;

    @BeforeClass
    public static void loadLibrary() {
        try {
            // A static method has to be called to run the static initializers (which load the native library).
            LibGit2.getJNACharset();
        } catch (Throwable t) {
            StackTraceElement[] st = t.getStackTrace();

            // Suppress the Throwable if it was thrown from LibGit2#getJNACharset as that method is not under test here.
            if (st.length == 0 || !st[0].getMethodName().equals("getJNACharset")) {
                throw t;
            } else {
                System.err.println("Suppressing " + t);
            }
        }
    }

    @Test
    public void git_libgit2_version() {
        LibGit2Version version = LibGit2.git_libgit2_version();

        String failFmt = "The git2 native library does not have the expected %s version. Expecting %d (is %d).";

        if (GIT2_EXPECTED_MAJOR >= 0) {
            String msg = String.format(failFmt, "major", GIT2_EXPECTED_MAJOR, version.major);
            assertEquals(msg, GIT2_EXPECTED_MAJOR, version.major);
        }

        if (GIT2_EXPECTED_MINOR >= 0) {
            String msg = String.format(failFmt, "minor", GIT2_EXPECTED_MINOR, version.minor);
            assertEquals(msg, GIT2_EXPECTED_MINOR, version.minor);
        }

        if (GIT2_EXPECTED_PATCH >= 0) {
            String msg = String.format(failFmt, "patch", GIT2_EXPECTED_PATCH, version.patch);
            assertEquals(msg, GIT2_EXPECTED_PATCH, version.patch);
        }

        System.out.println("The git2 version " + version + " is acceptable.");
    }

    @Test
    public void git_merge_file() {
        GitMergeFileOptions opts = new GitMergeFileOptions();
        GitMergeFileResult res = new GitMergeFileResult();

        String leftContent = "A";

        GitMergeFileInput left = new GitMergeFileInput();
        left.setContent(leftContent, UTF_8);
        opts.our_label = "Left";

        String baseContent = "";

        GitMergeFileInput base = new GitMergeFileInput();
        base.setContent(baseContent, UTF_8);
        opts.ancestor_label = "Base";

        String rightContent = "B";

        GitMergeFileInput right = new GitMergeFileInput();
        right.setContent(rightContent, UTF_8);
        opts.their_label = "Right";

        LibGit2.git_merge_file(res, base, left, right, opts);

        String expected = "<<<<<<< Left\n" +
                          "A\n" +
                          "=======\n" +
                          "B\n" +
                          ">>>>>>> Right\n";

        assertEquals(expected, res.getResult(UTF_8));
    }
}